---
layout: twioc.pug
title: "#1 This Week in OpenCode"
author: adhami
subtext: Update on what OpenCode members did this week from {{timespan}}.
thumbnail: /images/black_mux.png
thumbnail_alt: replace_me
date: {{today}}
tags: ["posts", "twioc"]
---

{{sections}}

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek-oc:matrix.mit.edu](https://matrix.to/#/#thisweek-oc:matrix.mit.edu) with updates on your own projects!
