cd /data

echo "Updating config.toml ..."
wget -nv -O config.toml https://gitlab.com/opencode-mit/nosy-config/-/raw/main/config.toml

echo "Updating report_template.md..."
wget -nv -O report_template.md https://gitlab.com/opencode-mit/nosy-config/-/raw/main/report_template.md

echo "Updating section_template.md..."
wget -nv -O section_template.md https://gitlab.com/opencode-mit/nosy-config/-/raw/main/section_template.md

echo "Updating project_template.md..."
wget -nv -O project_template.md https://gitlab.com/opencode-mit/nosy-config/-/raw/main/project_template.md

echo "Updating update_config.sh ..."
wget -nv -O update_config.sh https://gitlab.com/opencode-mit/nosy-config/-/raw/main/update_config.sh
